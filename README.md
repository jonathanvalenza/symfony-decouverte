###
instaler symfony :
1 - aller dans le repertoire ou vous souhaitez installer le projet

2 - entrer dans la console :
composer create-project symfony/skeleton decouverte-symfony

3 - pour installer ext-curl(nécéssaire pour symfony/website-skeleton), entré dans la console en administrateur :
sudo apt-get install php7.2-curl

4 - installez doctrine/orm et doctrine/doctrine-bundle en une seul fois, entrez dans la console :
composer require symfony/orm-pack

dans le fichier .env entrez les information de la base de donnée :
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name

==========
pense bête
==========

repository = DAO

entity = class représentant une table dans la BDD

routing = 
1/ config/route.yaml
2/ dans les commentaire d'une class du controller -> @Route("/exemple", name="exemple")
(Pour les route, il est possible de définir les methodes qui seront disponible pour cette page) (ex :  @Route("/exemple", name="artiexemplecle", methode={"GET", "POST"}))

créer un utilisateur : make:user 
cela crée une class utilisateur qui implemente userInterface et implémente la partie security. il faut implementer les attribut username, password, rôles.
pour définir les rôle il faut définir les role (ex : ROLE_LAMBDA, ROLE_ADMIN). Il est possible d'implémenter un mecanisme d'heritage.

Securité (Documentation : https://symfony.com/doc/current/security.html):
1/ installer le securitu bundel -> composer require symfony/security-bundle
2/ créer une class utilisateur -> php bin/console make:user, puis suivre les étapes

encoder manuellement un mot de passe -> php bin/console security:encode-password
définir la securisation : securitu.yaml ou annotation
acces-control = moins performant que les annotations
conseil : implémenter un securituControlleur.

###

==================
Tuto perso symfony
==================

documentation Symfony 4
-----------------------
==============
installation :
==============

1/ installer composer

créer un nouveau projet 
-----------------------

1/ en ligne de commande tapez : composer create project symfony/website-skeleton nom-du-projet 
(pour une installation optimal. Pour une installation minimal tapez : composer create project symfony/skeleton)

configurer la connexion à la database
------------------------------------

1/ dans le fichier .env, configurez  DATABASE_URL=mysql://nom_utilisateur:Password@127.0.0.1:3306/nom_de_la_database

lancer le serveur local
-----------------------

1/ en ligne de commande, tapez : 

php -s 127.0.0.1:8000 -t public
ou
php bin/console server run

voir la liste des commandes dans la console
-------------------------------------------

1/ en ligne de commande, tapez : php bin/console

============
Le routing :
============

definir une route (methode yaml)
--------------------------------

1/ ouvrez le fichier config/routes.yaml 
2/ definissez le nom de la route (ex: home:)
3/ définissez le chemin (ex: path: / <- pour pointer sur la racine)
4/ définissez le controller (ex: controller: App\Controller\NomDuController::methodeDuController)
5/ le tous nous donner :
home:
   path: /chemin
   controller: App\Controller\NomDuController::methodeDuController

créer un controller
-------------------
1/ dans le fichier src/controller, créez un fichier
2/ definissez le namespace (ex: namespace App\Controller;)
3/ créez une classe (ex: class NomDuController {})
4/ créez une methode public qui permetra, par exemple, d'affichez la page d'acceuil (ex: public function methodeDuController(){})
   4.1/ Vous pouvez spécifier à la methode qu'elle va retourner une response (ex:  public function methodeDuController():Response{}). Si vous faite cela, il faudra    importer : use Symfony\Component\HttpFoundation\Response;
5/ le tous donne :

namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
class NomDuController {

    /**
     * @return Response
     */
    public function methodeDuController():Response{
	return new Response(content 'Hello world')
    }
}

6/ testez sur un navigateur, aprés avoir démaré le server, si le resultat et bien celui attendu.

definir une route (methode annotation)
--------------------------------------

1/ installez le routing en ligne de commande (seulement si vous avez installé la version skeleton) :

composer require symfony/routing

2/ importez le namespace : 

use Symfony\Component\Routing\Annotation\route;

3/ en commentaire de vos methodes, dans vos controller, tapez : 

/**
 * @Route("/chemin", name="nomDeLaRoute")
 */

Cela donne :

namespace App\Controller;

use Symfony\Component\Routing\Annotation\route;
use Symfony\Component\HttpFoundation\Response;

class NomDuController {

    /**
     * @Route("/chemin", name="nomDeLaRoute")
     * @return Response
     */
    public function methodeDuController():Response{
	return new Response(content 'Hello world')
    }
}

fonctionnement des href dans les pages html
-------------------------------------------

Pour créer un href dans notre page html (pour la navbar par exemple, cela s'implémente comme ceci :

href="{{ path('nomDeLaRoute') }}"

==============
Optionel debut
==============
injectez twig dans le controller
--------------------------------

1/ pour cela rendez vous sur le fichier service.yaml
2/ créer un service pour un controller (ex: App\Controller\NomDuController)
3/ taguez le controller dans le service. ex : 

App\Controller\NomDuController
    tags: ['controller.service_arguments']

4/ définissez l'arguments Twig. ex: 

App\Controller\NomDuController
    tags: ['controller.service_arguments']
    arguments:
        $twig: '@twig'


============
Optionel fin
============

Afficher une page (methode twig)
--------------------------------

1/ Dans votre controlleur, importez twig environment. ex:

use Twig\Environment;

2/ dans votre controller, implémentez une propriété $twig et en anotation, précisez que c'est une variable de type twich\environement. ex :

class NomDuController {
    /**
     * @var twich\environement
     */
    private $twig;
}

3/ implémentez le contructeur de votre controller avec twig ex: 

public function __construct(Environment $twig) { 
   $this->twig = $twig 
}

4/ dans votre méthode, on va apeller la methode render de l'objet twig pour afficher le fichier de template. ex: 

public function methodeDuController():Response {
   return new Response($twig->twig->render('page/home.html.twig'));
}

Afficher une page (methode AbstractController)
----------------------------------------------

1/ importez AbstractController.  ex:

use Symfony\Bundle|FrameworkBundle\Controller\AbstractController;

2/ dans vos fonctions, utilisez la methode render. ex:

return $this->render('pages/nomDePage.html.twig')

Ceci donne :

namespace App\Controller;

use Symfony\Component\Routing\Annotation\route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle|FrameworkBundle\Controller\AbstractController;

class NomDuController extends AbstractController {

    /**
     * @Route("/chemin", name="nomDeLaRoute")
     * @return Response
     */
    public function methodeDuController():Response {
	return $this->render('pages/nomDePage.html.twig')
    }
}

rediriger vers une page
-----------------------

1/ pour rediriger vers une page, dansla methode de votre controller tapez :

return $this->redirectToRoute('nomDeLaRoute');

dans l'exemple ou la connexion de l'utilisateur a exprié, cela donnerai :

if (condition de teste d'autorisation == n'est pas bon) {
    return $this->redirectToRoute('login');
}

=============
le templating
=============

structure des pages
-------------------

Une structure de page existe déjà dans notre projet, elle ce trouve dans template/base.html.twig. cela fonctionne par block, un peu comme les includes et on peux retouver plusieurs type de block de départ (je pense que l'on peux créer ses propre block, a voir plus tard dans l'avancé du tuto). les block sont constitué comme cela :

{% block nomDuBlock %} {% endblock %}

base.html.twig est structuré comme une page HTML classique, avec un head et un body, avec des block a l'interrieur (

{% block Title %}{% endblock %}, 
{% block stylesheets%}{% endblock %}, 
{% block body%}{% endblock %}, 
{% block javascript%}{% endblock %}

). On pourra implémenter cette structure sur toutes nos nouvelles pages en faisant un extend de ce fichier.

Avant cela, nous allons installez bootstrap dans notre projet. récupérez le lien du CDN de bootstrap et implémentez le liens du css dans votre base.html.twig, juste avant la partie stylesheet. ex:

<link rel="stylesheet" href="http://liens.du.cdn.de.boostrap" etc...

et le lien du javascript en fin de body. ex:

<script src="https://lien.du.cdn.de.bootstrap" etc...

Dans cette page, vous pouvez implémenter toutes les structure qui seront commune au page qui vont extend base.html.twig.
Part exemple :
- La navbare
- le header
- etc...

EXTRA : pour définir dans la bare de nav, quel boutton dois être actif, voir la vidéo de grafikart -> Symfony 4 par l'exemple (2/16) -> time 20:40

créer une page
--------------

1/ créer un fichier dans template/pages/NomDeLaPage.html.twig
2/ implémenté en debut de page : {% extends 'base.html.twig' %}
3/ implémentez le block body : 

{% block body%}
Contenu du body
{% endblock %}

faire une boucle dans le template
---------------------------------

1/ implementez le template comme ceci :

{% for entité in entités %}
    <p>{{ entité.name }}</p>
    <p>{{ entité.description }}</p>
{% enflor %}

variables et tiré de votre entité, vous pouvez implémenter des proprieté de votre entité mais aussi implémenter des methodes. Dans le cas ou vos proprieté sont en private, symfony s'occupe automatiquement d'utiliser les getters de votre entité.

affichez des valeurs de notre entité
------------------------------------

1/votre page de template est lié par annotation à votre entité, lorsque vous avez envoyé à votre page la response de l'entité selectionner (entity[0]), pour afficher les valeur, cela ce présente comme ceci :

<p>{{ entity.name }}<p>
 
===============
Base de données
===============

créer une base de donnée
------------------------

1/ configurer la base de donnée (voir configurer la base de donnée dans la partie installation)
2/ en ligne de commande, tapez : 

php bin/console doctrine:database:create

Migrer la base de donnée
------------------------

1/ entrez en ligne de commande :

php bin/console make:migration

cette commande va lire vos entité, comparer vos entité avec la base de donnée et créer un fichier de migration dans src/Migration/Version123456789.php. Dans ce fichier sera créer les commande SQL qui va mettre à jour votre base de donnée afin quel corresponde avec les entité de votre projet.

2/ Lorsque vous avez verifié que tous est en ordre, entrez en ligne de commande :

php bin/console doctrine:migration:migrate

3/ la console va vous demander de confirmer les changements, entrez yes ou no si vous n'êtes pas sur (changer la base de donnée semble iréversible).

Maintenant, la base de donnée devrai contenir les tables corespondante a vos entité, ainsi qu'une table contenant le numéro de version de votre base de donnée.

Ajoutez une entré en base de donnée
-----------------------------------

1/ Dans le controller (ou le repository ?), créer votre entité. ex :

public function nomDeMethode(): Response {
    $entite = new NomDeLEntité;
    $entite->setPropriete1('valeur')->setPropriete2('valeur');
}

2/ Vous devez persister votre entité. cela ce présente comme ceci :

$nomDeVariable = $this->getDoctrine()->getManager;
$nomDeVariable->persist($entite);

ensuite, vous devez porter les changements de EntityManager dans votre base de donnée avec la methode flush. ex:

$nomDeVariable->flush();

3/ Maintenant, il ne vous reste plus qu'a apellé la methode du controller.

Recupérer des informations en base de donnée (dans une methode)
---------------------------------------------------------------

1/ il faut importer l'entité dans le controller

use App\Entity\NomDeLEntite;

2/ Dans la methode de votre controller, il faut initialiser votre repository via doctrine et l'implementer dans une variable. ex:

$repository = $this->getDoctrine()->getRepository(nomdeLEntite::class);

3/ utilisez la variable pour vos actions ex:

dump($repository)

Pour savoir quel repository utiliser, symfony utilise les annotations. dans l'entité, tous au debut, il y a une annotation qui specifie pour cette entité le repository a utiliser.

Recupérer des informations en base de donnée (dans le construct)
---------------------------------------------------------------

1/ il faut importer l'entité et le Repository dans le controller 

use App\Entity\NomDeLEntite;
use App\Repository\NomDeLEntiteRepository;

2/ créer le contructeur et implementez le Repository. ex:

public function __contruct(NomDeLEntiteRepository $repository) {}

3/ créer une proprieté ex: 

/**
 * @var NomDeLEntiteRepository
 */
private $repository;

4/ Dans le contructeur, ajoutez : $this->repository = $repository;

5/ utilisez la proprieté, 
    5.1/ pour récupérer rapidement un enregistrement, il existe la methode find(ID), findAll(), findOneBy(['critere' => valeur])

Ceci donne :

/**
 * @var NomDeLEntiteRepository
 */
private $repository;

public function __contruct(NomDeLEntiteRepository $repository) {
    $this->repository = $repository;
}

/**
 * @return response
 */
public function index(): Response {
    $this->repository->findAll();
    dump($property);
}

Ci vous souhaitez créer vos propres methodes, dans le cas ou find() ne sois pas sufisant par exemple, dans le repository ce trouve en commentaire un exemple de la structure d'une methode à créer. voici un exemple pour une methode qui renvoie les informations celon si un ereservation a été payé ou non :

/**
 * @return array
 */
public function resaPayé(): array {
    return $this->createQueryBuilder(alias 'resa')
        ->where('resa.reglé = true')
        ->getQuery()
        ->getResult();
}


Mettre a jour une entité
------------------------

1/ il faut recupérer l'objectManager. ex:

/**
 * @return array
 */
private $manager

public function __contruct(NomDeLEntiteRepository $repository, ObjectManager $manager) {
    $this->manager = $manager;
}

2/ a partir de la, vous pouvez utiliser votre propriete manager. ex:

/**
 * @return response
 */
public function index(): Response {
    $reservation = $this->repository->findAll();
    $reservation[0]->setReglé(true);
    $this->manager->flush();
}


==================
Les class (entity)
==================

créer une entité
----------------

1/ en ligne de commande, tapez :

php bin/console make:entity

La class va être créer dans src/Entity/nomDeLaClass, avec les annotations, les getters et les setters pour chaque proprieté et un repository sera aussi créer dans src/repository/nomDeLaClassRepository

2/ Entrez le nom de l'attribut que vous souhaitez implémentez dans votre class. (ex: id, name, title, etc...)
3/ Entrez le type de la proprieté (pour voir la liste des possibilité de type, vous pouvez tapez : ?) (ex : string, integer, datetime, manytomany, etc..)
4/ Entrez les détails de votre proprieté (ex: pour une string, il vous sera demandé la longueur (255 part defaut)
5/ Entrez si cette proprieté peux etre null en base de donnée) (ex: yes, no)
6/ Enfin, il vous sera demandé si vous souhaitez rajouter une nouvel proprieté, si oui, repetez l'opération de création de class.
7/ Si vous souhaitez ajouter une option à votre proprieté (comme la valeur par defaut), dans l'annotation de cette propriété, ajoutez 

options={"default":l'option}

ex:

/**
 * @ORM\Column(type="boolean", option={"default": false})
 */

8/ Vous pouvez ajouter un constructeur à votre entité (manuellement) si vous souhaitez que des actions ce face lors de la création de l'entité (pour implémenté une date par exemple).

public function __construct() {
    $this->proprieteDate = new \DateTime();
}

editer une entité (methode manuel)
----------------------------------

Si vous souhaitez ajouter des proprieté a votre entité : 
1/ Ajoutez manuellement la proprieté avec ses annotations, son getter et son setter.

editer une entité (ligne de commande)
-------------------------------------

1/ en ligne de commande, tapez :

php bin/console make:entity NomDeLaClass

2/ La console va vous demandez si vous souhaitez rajouter des champs (c'est ce que l'on souhaite) et la methode de fonctionnement est le même que si vous créée une entité (nom de la propriété, type, etc...)


==============
Les forulaires
==============

conseil :
Pour acceder à la valeur du tableau que vous souhaité editer a l'aide d'un formulaire, dans l'affichage de votre liste, créer un bouton avec en Href :

href="{{ path('nomDeLaRoute', {id: nomDeLEntite.id}) }} 

Ensuite, dans votre controller, créez une methode avec en annotation la route :

    /**
     * @Route("/chemin/{id}", name="nomDeLaRoute")
     * @return Response
     */
    public function methodeDuController(Entite $entite):Response {
	return $this->render('pages/nomDePage.html.twig', compact('entite'));
    }

Créer un formulaire
-------------------

1/ Pour créer un formulaire, rendez vous dans la console et en ligne de commande tapez :

php bin/console make:form

2/ il vous sera demandé le nom de votre formulaire, tous les nom devrons finir par 'type' ex:

NomDeLEntiteType

3/ il vous est demandé le nom de l'entité qui correspond :

NomDeLEntite

4/ c'est tous pour la console, maintenant, un dossier avec vos formulaire a été créer dans votre projet et a l'interrieur ce trouve votre fichier formulaire pour votre entité avec a l'interrieur la class que vous venez de créer avec une methode buildForm qui contient un tableau avec toutes les proprieté de votre entité que vous souhaitez afficher (s'il y a des proprieté que vous ne souhaitez pas mettre dans votre formulaire, retirez le du tableau) et une methode configureOption qui vous permettra de configurer votre formulaire.

namespace App\form;

public function buildForm(FormBuilderInterface $builder, array $option) {
    $builder
        ->add(child 'title')
        ->add(child 'description')
    ;
}

afficher un fomulaire
---------------------

1/ Dans votre controller, a l'interrieux de la methode qui renvoie la vue ou vous souhaitez afficher le formulaire, utilisez la methode createForm avec en premier parametre la class que vous venez de générer (ex: NomDeEntiteType) et en second parametre les données, ça peux etre une entité ou un tableau. (ex: call, $entité) puis enregistrez le dans une variable. ex :

    /**
     * @Route("/chemin/{id}", name="nomDeLaRoute")
     * @param NomDeLEntite $NomDeLEntite
     * @return Response
     */
    public function methodeDuController(NomDeLEntite $NomDeLEntite):Response {
        $form = $this->createForm(type: NomDeLEntiteType::class, $NomDeLEntite); 
	return $this->render('pages/nomDePage.html.twig', [
            'NomDeLEntite' => $NomDeLEntite,
            'form' => $form->createView()
        ]);
    }

2/ Dans votre vue vous devez implémentez le formulaire comme ceci :

{{ form_start(nomDuForm) }}
    {{ form_rest(nomDuForm) }}
{{ form_end(nomDuForm) }}

cela affichera automatiquement le formulaire dans votre vue

Si vous implementez un bouton a l'interrieur de ce formulaire, il utilisera la methode POST (a vérifier)

mettre le form en forme
-----------------------

Actuellement, le formulaire et posé comme ça, sans aucune structure et ce n'est pas trés beau (voir degueulasse)

1/ dans le fichier config/package/twig.yaml, on va spécifier le type de theme a utiliser pour nos formulaire. Nous allons utiliser bootstrap 4 :

form_themes: ['bootstrap_4_layout.html.twig']

La liste des theme sont disponible dans : vendor/symfony/twig-bridge/Ressources/views/form

2/ Utilisez les class CSS Bootstrap pour finaliser la mise en form avec des class row, col-md, etc... et pour définir quel form est afficher ou, ceci ce présente comme cela :

<div class="row">
    <div class="col-md-6"> {{ form_row(form.propriete) }}</div>
</div>

changer les options (comme le label) d'un form
----------------------------------------------

1/ pour changer le label ou une autre option d'un formulaire, rendez vous dans le fichier de votre formulaire et dans le tableau, ajoutez des option au formulaire souhaité. ex:

namespace App\form;

public function buildForm(FormBuilderInterface $builder, array $option) {
    $builder
        ->add(child 'title', [
	    'label' => 'titre'
	])
        ->add(child 'description')
    ;
}

la liste des option se trouve sur la doc (placeholder, attr, choiceType...)

Editer un element par formulaire
--------------------------------

1/ rendez vous dans le controller de votre formulaire et on va injecter Request a notre methode.

    public function methodeDuController(NomDeLEntite $NomDeLEntite, Request $request)

2/ juste en dessous de la ligne qui crée votre formulaire nous allons implémenter la gestion du formulaire et passer request en argument.

$form->handleRequest($request);

3/ nous avons besoin d'injecter dans le construct l'objectManager et l'apeller $em (__construct(ObjectManager $em)) avec la propriete private $em

4/ nous allons tester si le formulaire a été envoyé et est-ce qu'il est valide. si tous va bien on va utiliser la methode flush

if ($form->isSubmittes() && $form->isValid()) {
    $this->em->flush();
}

5/ enfin, il ne nous reste plus qu'a rediriger vers la page. Tous cela donne :

/**
* @Route("/chemin/{id}", name="nomDeLaRoute.edit")
* @param NomDeLEntite $NomDeLEntite
* @param Request $request
* @return \symfony\Component\HttpFoundation\Response
*/
public function methodeDuController(NomDeLEntite $NomDeLEntite, Request $request) {
    $form = $this->createForm(type: NomDeLEntiteType::class, $NomDeLEntite);
    $form->handleRequest($request);

    if ($form->isSubmittes() && $form->isValid()) {
        $this->em->flush();
        return $this->redirectToRoute(route: 'nomDeLaRoute');
    }
    return $this->render('pages/nomDePage.html.twig', [
        'NomDeLEntite' => $NomDeLEntite,
        'form' => $form->createView()
    ]);
}


Créer un element par formulaire
-------------------------------

1/ créer une nouvel methode dans votre controller qui va créer un nouvel element

/**
* @Route("/chemin/{id}", name="nomDeLaRoute")
*/
public function new() {
	$entite = new Entite();
}

Puis on va utiliser la même mecanique que pour l'edition, c'est a dire 

1.1/ on va crée un formulaire en lui passant l'entité, 
1.2/ on va demander au formulaire de gérer la requet (sauf que cette fois nous n'allon pas utiliser l'entityManager mais nous allons persisté notre nouvel entité) avec : 

$this->em->persist($nomDeLEntite);

1.3/ pour permettre de persister, nous devons injecter la requete dans la methode 

public function new(Request $request) {}

1.4/ enfin, si tous a fonctionner, nous allons renvoyer la vue  

2/ Cela donne :

/**
* @Route("/chemin/{id}", name="nomDeLaRoute.create")
* @param NomDeLEntite $NomDeLEntite
* @param Request $request
* @return \symfony\Component\HttpFoundation\Response
*/
public function new(Request $request) {
    $entite = new NomDeLEntite();
    $form = $this->createForm(type: NomDeLEntiteType::class, $NomDeLEntite.new);
    $form->handleRequest($request);

    if ($form->isSubmittes() && $form->isValid()) {
	$this->em->persist($nomDeLEntite);
        $this->em->flush();
        return $this->redirectToRoute(route: 'nomDeLaRoute');
    }
    return $this->render('pages/nomDePage.html.twig', [
        'NomDeLEntite' => $NomDeLEntite,
        'form' => $form->createView()
    ]);
}

supprimer un element
--------------------

La façon de prosséder va ressembler au 2 autres 

1/ On va commencer par créer une methode delete avec en argument l'entité:

public function delete(NomDeLEntite $nomDeLEntite) {}

2/ on implémente les annotations et dans l'annotation, nous allons specifier que cette methode n'accepte que le verbe HTTP DELETE :

/**
* @Route("/chemin/{id}", name="nomDeLaRoute.delete", methods="DELETE")
* @param NomDeLEntite $NomDeLEntite
* @return \symfony\Component\HttpFoundation\RedirectResponse
*/

3/ on recupère l'entityManager et on utilise la methode remove et on lui donne en parametre l'entité a supprimer

$this->em->remove($nomDeLEntite);

4/ une fois supprimer, on va mettre a jour la base de donnée (avec flush)

$this->em->flush();

5/ puis on redirige l'utilisateur

return $this->redirectToRoute(route: 'nomDeLaRoute');

Voila, le code est simple mais plusieurs probleme ce pose, nous allons y remedier.

La methode DELETE dans l'annotation pose quelques probleme, pour y remedier, nous allons implementer un mini formulaire dans notre formulaire :
-----------------------------------------------------------------------------------------------------------------------------------------------

1/ dans notre vue, juste en dessous du boutton d'edition un mini formulaire avec en action, le path de notre delete et l'id de notre entité et en method la method post.

<form method="post" action="{{ path('nomDeLaRoute.delete', {id: entite.id} }} onsubmit="return confirm('Voulez vous vraiment supprimer cette element ?')">

2/ ensuite, on va lui ajouter un champ caché et comme name, on va lui donnée la methode:

<input type="hidden" name="_method" value="DELETE">

3/ puis on implemente un boutton

Cela fonctionne mais cela pose un probleme de securité (jeton CSRF). nous allons y remedier

4/ ajouter un nouveau champ a votre formulaire (pour concatener avec twig on utilise ~

<input type="hidden" name="_token" value="{{ csrf_token('delete' ~ nomDeLEntite.id) }}">

5/ maintenant, nous allons retourner dans le controller verifier que le token est valide. Nous allons d'abord ajouter request en argument de la methode puis tester ça dans votre methode delete

if($this->isCsrfTokenValid(id 'delete' . $nomDeLEntite->getId(), $request->get(key: '_token'))) {}

6/ c'est un petit peu long a implémenter, il est judicieux de créer une methode déjà toute faite pour pouvoir la reutiliser a volonté. Une fois la condition terminé, on peux mettre le reste du code dans le corp de la condition. cela donne au final :

/**
* @Route("/chemin/{id}", name="nomDeLaRoute.delete", methods="DELETE")
* @param NomDeLEntite $NomDeLEntite
* @return \symfony\Component\HttpFoundation\RedirectResponse
*/
public function delete(NomDeLEntite $nomDeLEntite, Request $request) {
	if($this->isCsrfTokenValid(id 'delete' . $nomDeLEntite->getId(), $request->get(key: '_token'))) {
		$this->em->remove($nomDeLEntite);
		$this->em->flush();
	}
	return $this->redirectToRoute(route: 'nomDeLaRoute');
}

methode flash
-------------

1/ Cette methode sert a envoyé un petit message lors du succes de notre création, edition ou suppression. cela ce présente comme ceci :

$this->addFlash(type: 'succes', message: 'votre entite a bien été créer/modifié/supprimer.

cette methode est implémenté juste aprés la methode flush. Cela permet a l'utilisateur d'avoir une confirmation de son action.

2/ Maintenant, nous allons implémenté dans notre vue le message. A l'endrois ou vous souhaitez afficher le message, tapez :

{% for message in app.flashes('success') %}
	<div class="alert alert-success">
		{{ message }}
	</div> 
{% endflor %}


